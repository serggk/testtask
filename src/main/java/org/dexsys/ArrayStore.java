package org.dexsys;

import java.util.*;
import java.util.stream.Collectors;

public class ArrayStore {
    private HashMap<String, List<Integer>> arraysOfMultiples = new HashMap<String, List<Integer>>();
    private boolean anyMore = false;

    public static ArrayStore INSTANCE = new ArrayStore();


    public void setInitArray(List<Integer> initArray, Map<String, Integer> dividers) {
        this.arraysOfMultiples.clear();
        dividers.keySet().stream().forEach((p) -> this.arraysOfMultiples.put(p, new LinkedList<Integer>()));
        this.anyMore = initArray.stream()
                .sorted()
                .map((el) -> {
                    return dividers.entrySet().stream()
                            .map((divider) -> {
                                if (el % divider.getValue() == 0) {
                                    List<Integer> aaa = this.arraysOfMultiples.get(divider.getKey());
                                    aaa.add(el);
                                    this.arraysOfMultiples.put(divider.getKey(), aaa);
                                    return false;
                                }
                                return true;
                            })
                            .reduce(false, (acc, p) -> acc || p);
                })
                .reduce(false, (acc, p) -> acc || p);
        System.out.println(this.arraysOfMultiples);
    }

    public List<Integer> getArrayOfMultiples(String divider) {
        return this.arraysOfMultiples.getOrDefault(divider, new ArrayList<Integer>());
    }

    public HashMap<String, List<Integer>> getAllArraysOfMultiples() {
        return arraysOfMultiples;
    }

    public boolean isAnyMore() {
        return anyMore;
    }

    public void clearArray(String divider) {
        this.arraysOfMultiples.put(divider, new LinkedList<Integer>());
    }

    public List<Integer> merge() {
        List<Integer> aaa = arraysOfMultiples.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        arraysOfMultiples.keySet().stream().forEach((p) -> this.arraysOfMultiples.put(p, new LinkedList<Integer>()));
        return aaa;
    }
}
