package org.dexsys;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App 
{
    private final HashMap<String, Integer> dividers = new HashMap<String, Integer>();
    private final ArrayStore arraysStore = ArrayStore.INSTANCE;

    private App() {
        dividers.put("x", 3);
        dividers.put("s", 7);
        dividers.put("m", 21);
    }

    private void dispatcher(String action) {
        String[] params = action.toLowerCase().split(" ", 2);
        switch (params[0]) {
            case "init":
                arraysStore.setInitArray(
                        Arrays.stream(params[1].split(" "))
                                .map(Integer::new)
                                .collect(Collectors.toList()),
                        this.dividers);
                break;
            case "print":
                if (params.length == 1) {
                    arraysStore.getAllArraysOfMultiples().entrySet()
                            .stream()
                            .map((p)->{
                                if (p.getValue().isEmpty())
                                    return String.format("Список %s пуст", p.getKey());
                                return String.format("Список %s %s", p.getKey(), p.getValue().toString());
                            })
                            .forEach(System.out::println);
                } else {
                    List a = arraysStore.getArrayOfMultiples(params[1]);
                    if (a.isEmpty())
                        System.out.println(String.format("Список %s пуст", params[1]));
                    else
                        System.out.println(String.format("Список %s %s", params[1], a.toString()));
                }
                break;
            case "anymore":
                System.out.println(arraysStore.isAnyMore());
                break;
            case "clear":
                arraysStore.clearArray(params[1]);
                break;
            case "merge":
                System.out.println(arraysStore.merge());
                break;
            case "exit":
                System.out.println("Good bye!!!");
                break;
            default:
                System.out.println("Unknown command");
        }
    }

    public static void main( String[] args )
    {
        App application = new App();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        in.lines()
                .peek(application::dispatcher)
                .anyMatch((p) -> p.toLowerCase().equals("exit"));
    }
}
